svnkit (1.8.14-3) unstable; urgency=medium

  * Team upload.
  * Depend on libequinox-common-java and libeclipse-core-runtime-java
    instead of eclipse-rcp
  * Fixed the watch file
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 24 Sep 2018 00:15:45 +0200

svnkit (1.8.14-2) unstable; urgency=medium

  * Team upload.

  [ Miguel Landaeta ]
  * Remove myself from uploaders list. (Closes: #871890)
  * Simplify d/rules.
  * Update copyright information.

  [ Markus Koschany ]
  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.1.4.
  * Fix FTBFS with libsvn-java 1.10.0-1. (Closes: #897132)
  * Use source/target 1.7.

 -- Markus Koschany <apo@debian.org>  Fri, 11 May 2018 23:56:14 +0200

svnkit (1.8.14-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.8.14.

 -- Markus Koschany <apo@debian.org>  Sun, 16 Oct 2016 23:10:04 +0200

svnkit (1.8.13-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.8.13.
  * Switch to compat level 10.
  * Declare compliance with Debian Policy 3.9.8.
  * svnkit: Add alternative java7-runtime-headless dependency.
  * Drop libsvn-java-1.9.2.patch. Fixed upstream.
  * Rebase script-fix-classpath.patch and merge script-add-shebang.patch into
    it.
  * Repack the tarball and remove all jar files except the template jar
    files. Remove the embedded copy of nailgun.

 -- Markus Koschany <apo@debian.org>  Sun, 09 Oct 2016 16:23:39 +0200

svnkit (1.8.12-1) unstable; urgency=medium

  * Team upload.
  * Imported Upstream version 1.8.12.
  * Update copyright years.
  * Vcs: Use https.
  * Declare compliance with Debian Policy 3.9.7.

 -- Markus Koschany <apo@debian.org>  Fri, 01 Apr 2016 06:41:37 +0200

svnkit (1.8.11-1) unstable; urgency=medium

  * Team upload.
  * Imported Upstream version 1.8.11.
  * Add libsvn-java-1.9.2.patch and implement abstract methods to prevent a
    FTBFS with version 1.9.2 of libsvn-java. (Closes: #802356)
  * Rebase script-add-shebang.patch.
  * Remove svnkit.lintian-overrides.
  * Fix Lintian warning space-in-std-shortname-in-dep5-copyright.
  * Vcs-Browser: Use https.
  * Tighten dependencies on libsvn-java to >= 1.9.2.

 -- Markus Koschany <apo@debian.org>  Tue, 08 Dec 2015 23:44:43 +0100

svnkit (1.8.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Removed the unnecessary Maven dependency on net.java.dev.jna:platform
    and com.jcraft:jsch.agentproxy.* (this broke libshib-common-java,
    see #752964)
  * Depend on libsqljet-java >= 1.1.10-1 to ensure the Maven artifacts for
    sqljet are available and thus avoid breaking Maven builds.
  * debian/orig-tar.sh:
    - Use XZ compression for the upstream tarball
    - Use the version passed by uscan instead of the current version
  * Standards-Version updated to 3.9.6 (no changes)

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 17 Oct 2014 13:59:10 +0200

svnkit (1.8.5+dfsg-2) unstable; urgency=low

  * Fix typo with my email address in Uploaders list.
  * Update fix-logging-path.patch.
  * Fix override disparities regarding package section.

 -- Miguel Landaeta <nomadium@debian.org>  Sat, 03 May 2014 21:30:00 -0300

svnkit (1.8.5+dfsg-1) unstable; urgency=low

  * Team upload.

  [ Emmanuel Bourg ]
  * Standards-Version updated to 3.9.5 (no changes).
  * Switch to debhelper level 9.
  * Wrap and sort.

  [ Markus Koschany ]
  * Imported Upstream version 1.8.5+dfsg. (Closes: #743213)
    - Fixes FTBFS. (Closes: #735763)
  * debian/control: Canonicalize VCS-fields.
  * debian/control:
    - svnkit: Remove alternative dependency on java5-runtime-headless.
    - Tighten versioned build-dependency on libsvn-java to 1.8.8.
    - Update versioned dependency for trilead-ssh2 to 6401+svn158.
  * Drop fix-build-javac-1.6.0_24.patch. Java7 is the default now.
  * Rebase fix-logging-path.patch.

  [ Miguel Landaeta ]
  * Update my email address.
  * Remove outdated to-do list.
  * Remove outdated d/README.Debian file.
  * Install maven artifacts. (Closes: #740608).
  * Update d/orig-tar.sh script.

 -- Miguel Landaeta <nomadium@debian.org>  Sat, 05 Apr 2014 12:03:22 -0300

svnkit (1.7.5+dfsg-2) unstable; urgency=low

  * Removed obsolete DM-Upload-Allowed field from d/control.
  * Uploading this version into sid fixes FTBFS (Closes: #713784).

 -- Jakub Adam <jakub.adam@ktknet.cz>  Fri, 28 Jun 2013 15:11:50 +0200

svnkit (1.7.5+dfsg-1) experimental; urgency=low

  * New upstream release.
  * Update format of d/copyright.
  * Bump Standards-Version to 3.9.4.
  * Use ant with our custom build.xml as a buildsystem, upstream now
    uses gradle.
  * Remove contrib/sequence/* from d/copyright. These sources are now
    provided in a different package.
  * Add Jakub Adam to Uploaders.
  * Add #! sequence at the beginning of executable scripts.
  * Fix classpath in executable scripts.

 -- Jakub Adam <jakub.adam@ktknet.cz>  Fri, 21 Sep 2012 18:33:05 +0200

svnkit (1.3.5+dfsg-4) unstable; urgency=low

  * Drop the patch that fixes an FTBFS with subversion 1.7.5 because it didn't
    migrate to wheezy and now is causing an FTBFS with 1.6.x. (Closes: #684853).

 -- Miguel Landaeta <miguel@miguel.cc>  Tue, 14 Aug 2012 19:22:32 -0430

svnkit (1.3.5+dfsg-3) unstable; urgency=low

  * Team upload.
  * Actually add the DMUA flag to d/control.

 -- Niels Thykier <niels@thykier.net>  Thu, 21 Jun 2012 21:45:29 +0200

svnkit (1.3.5+dfsg-2) unstable; urgency=low

  [ Miguel Landaeta ]
  * Fix FTBFS due to changes introduced in subversion 1.7.5. (Closes: #678308).
  * Bump Standards-Version to 3.9.3. No changes were required.
  * Update copyright file.

  [ Niels Thykier ]
  * Ensure backward compatible byte code.
  * Set DMUA flag.

 -- Miguel Landaeta <miguel@miguel.cc>  Thu, 21 Jun 2012 21:21:51 +0200

svnkit (1.3.5+dfsg-1) unstable; urgency=low

  [Miguel Landaeta]
  * New upstream release.
  * Bump Standards-Version to 3.9.2. No changes were required.
  * Correctly order the arguments in dh7 invocation.
  * Update dates in copyright file and make it DEP-5 compliant.
  * Wrap properly Build-Depends list.
  * Fix/override several lintian warnings about missing classpath
    declarations and empty directories.

  [tony mancill]
  * Tweak package descriptions in debian/control.

 -- Miguel Landaeta <miguel@miguel.cc>  Wed, 20 Jul 2011 21:35:25 -0430

svnkit (1.3.3+dfsg-1) unstable; urgency=low

  * New upstream release.
  * svnkit now just depends on default-jre-headless.
  * Drop JRE dependences for libsvnkit-java as Java libraries are no longer
    required to depend on a JVM.

 -- Miguel Landaeta <miguel@miguel.cc>  Tue, 04 May 2010 23:49:44 -0430

svnkit (1.3.2+dfsg-1) unstable; urgency=low

  * Initial release (closes: #570867).

 -- Miguel Landaeta <miguel@miguel.cc>  Wed, 10 Mar 2010 20:08:02 -0430
