#!/bin/sh

set -e

# called by uscan with '--upstream-version' <version> <file>
VERSION=$2
package=`dpkg-parsechangelog | sed -n 's/^Source: //p'`
debian_version=`dpkg-parsechangelog | sed -ne 's/^Version: \(.*+dfsg\)-.*/\1/p'`
TAR=../${package}_${VERSION}.orig.tar.xz
DIR=${package}-${VERSION}.orig

# clean up the upstream tarball
unzip -d orig $3
mv "$(find orig -maxdepth 1 -type d ! -name orig)" orig/$DIR
XZ_OPT=--best tar -C orig -c -v -J -f $TAR   \
    --exclude doc/javadoc                    \
    --exclude '*.class'                      \
    --exclude contrib/javahl                 \
    --exclude contrib/jna                    \
    --exclude contrib/junit                  \
    --exclude contrib/maven                  \
    --exclude contrib/sqljet                 \
    --exclude contrib/trilead                \
    --exclude gradle-wrapper.jar             \
    --exclude svnkit-test/nailgun             \
    --exclude 'nailgun-*.jar'                \
    --exclude '*.exe'                        \
    --numeric-owner --group 0 --owner 0 $DIR
rm -rf $3 orig
